package org.academiadecodigo.bootcamp.webserver;

import org.academiadecodigo.bootcamp.webserver.request.ClientHandler;
import java.io.*;
import java.net.ServerSocket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.logging.Logger;

public class WebServer {


    ExecutorService threadPool;
    public static void main(String[] args) {
        try{

            WebServer webServer = new WebServer(8080);
            webServer.start();

        }catch (IOException ex){

            System.out.println("IOException caught "+ex.getMessage());
        }

    }
    private Logger logger = Logger.getLogger(WebServer.class.getName());
    private ServerSocket socket;

    public WebServer(int port) throws IOException {
        socket = new ServerSocket(port);
    }

    public void start() {
        logger.info("Listening for connections on port " + socket.getLocalPort());

        while (!socket.isClosed()) {


            try {

                threadPool = Executors.newCachedThreadPool();
                threadPool.submit(new ClientHandler(socket.accept()));
                //Thread thread = new Thread(new ClientHandler(socket.accept()));
                //thread.start();

            } catch (IOException e) {

                logger.severe("Error handling client request: " + e.getMessage());
            }
        }

    }


}