package org.academiadecodigo.bootcamp.webserver;

import java.io.File;

public enum MimeType {
    JPG("image"),
    JPEG("image"),
    PNG("image"),
    GIF("image"),
    HTML("text"),
    TXT("text", "plain"),
    UNSUPPORTED("application", "octet-stream");

    private String prefix;
    private String type;

    public static MimeType getFromExtension(String ext) {
        for (MimeType type : values()) {
            if (!type.name().toLowerCase().equals(ext)) {
                continue;
            }

            return type;
        }

        return UNSUPPORTED;
    }

    public static MimeType getFromFile(File resource) {
        String filename = resource.getName();
        String ext = filename.substring(filename.lastIndexOf(".") + 1);
        return getFromExtension(ext);
    }

    MimeType(String prefix, String type) {
        this.prefix = prefix;
        this.type = type;
    }

    MimeType(String prefix) {
        this.prefix = prefix;
        this.type = name().toLowerCase();
    }

    @Override
    public String toString() {
        return this.prefix + "/" + this.type;
    }
}
