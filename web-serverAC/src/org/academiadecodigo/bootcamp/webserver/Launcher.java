package org.academiadecodigo.bootcamp.webserver;

import java.io.IOException;
import java.util.concurrent.ThreadPoolExecutor;

public class Launcher {

    private static final int DEFAULT_PORT = 8080;

    public static void main(String[] args) {
        int port = DEFAULT_PORT;


        if (args.length > 0) {
            try {
                port = Integer.parseInt(args[0]);
            } catch (NumberFormatException e) {
                System.err.println(args[0] + " is not a valid port number");
                return;
            }
        }

        try {
            WebServer server = new WebServer(port);
            server.start();

        } catch (IOException e) {
            System.err.println("Unable to start server: " + e.getMessage());
        }
    }
}
