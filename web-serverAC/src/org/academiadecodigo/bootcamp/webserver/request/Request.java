package org.academiadecodigo.bootcamp.webserver.request;

import java.io.File;

public class Request {

    private Verb verb;
    private File resource;

    public Request(Verb verb, File resource) {
        this.verb = verb;
        this.resource = resource;
    }

    public Verb getVerb() {
        return verb;
    }

    public File getResource() {
        return resource;
    }
}
