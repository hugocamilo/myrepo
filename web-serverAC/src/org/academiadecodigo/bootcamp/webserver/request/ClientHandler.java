package org.academiadecodigo.bootcamp.webserver.request;

import org.academiadecodigo.bootcamp.webserver.MimeType;
import java.io.*;
import java.net.Socket;
import java.util.logging.Logger;

public class ClientHandler implements Runnable {

    private static final String ROOT_DIR = "www";
    private static final String INDEX_PAGE = "index.html";
    private static final File NOT_FOUND = new File(ROOT_DIR + "/not_found.html");
    private static final File UNSUPPORTED = new File(ROOT_DIR + "/unsupported.html");
    private Logger logger;
    private Socket client;

    public ClientHandler(Socket client) {

        this.client = client;
        logger = Logger.getLogger(ClientHandler.class.getName());
    }

    @Override
    public void run() {
        try {

            logger.info("Handling client " + client.getInetAddress() + ":" + client.getPort());
            Request request = parse(client.getInputStream());
            logger.info("Received " + request.getVerb() + " request for resource " + request.getResource().getName());
            serve(request, new DataOutputStream(client.getOutputStream()));
            close(client);

        } catch (IOException ex) {
            System.out.println("IOException caught " + ex.getMessage());
        }catch (InvalidHeaderException ex){
            System.out.println("Invalid Header Exception caught ");
        }

    }

    private Request parse(InputStream inputStream) throws InvalidHeaderException {
        String header = readRequest(inputStream);
        String[] headerParts = header.split(" ");

        if (headerParts.length < 2) {
            throw new InvalidHeaderException();
        }

        System.out.println(headerParts[1] + "------------");

        Verb verb = Verb.getFromString(headerParts[0]);
        File file = new File(ROOT_DIR + headerParts[1]);

        return new Request(verb, file);
    }

    private String readRequest(InputStream inputStream) {
        BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
        StringBuilder builder = new StringBuilder();

        try {
            String line = reader.readLine();

            while (line != null && !line.isEmpty()) {
                builder.append(line).append("\n");
                line = reader.readLine();
            }

        } catch (IOException e) {
            logger.warning("Error reading client's request: " + e.getMessage());
        }

        return builder.toString();
    }

    private void serve(Request request, DataOutputStream out) throws IOException {
        File file = request.getResource();
        int status = 200;

        if (file.isDirectory()) {
            file = new File(file.getPath() + "/" + INDEX_PAGE);
            System.out.println("DIRECTORY " + file.getPath());
        }

        if (!file.exists()) {
            System.out.println("ENTREI AQUI");
            file = NOT_FOUND;
            status = 404;
        }

        if (request.getVerb() == Verb.UNSUPPORTED) {
            file = UNSUPPORTED;
            status = 405;
        }

        logger.info("Sending response with " + status + " status and file " + file.getName());
        sendHeader(out, status, file);
        sendFile(out, file);
    }

    private void sendHeader(DataOutputStream out, int status, File file) throws IOException {
        out.writeBytes("HTTP/1.0 " + status + " \r\n");
        out.writeBytes("Content-Type: " + MimeType.getFromFile(file) + "\r\n");
        out.writeBytes("Content-Length: " + file.length() + " \r\n");
        out.writeBytes("\r\n");
    }

    private void sendFile(DataOutputStream out, File file) throws IOException {
        BufferedInputStream in = new BufferedInputStream(new FileInputStream(file));
        byte[] buffer = new byte[1024];
        int bytes;

        try {
            while ((bytes = in.read(buffer)) != -1) {
                out.write(buffer, 0, bytes);
            }

        } finally {
            in.close();
        }
    }

    private void close(Socket socket) {
        if (socket == null) {
            return;
        }

        try {
            socket.close();

        } catch (IOException e) {
            logger.severe("Error closing client socket: " + e.getMessage());
        }
    }


}

}
