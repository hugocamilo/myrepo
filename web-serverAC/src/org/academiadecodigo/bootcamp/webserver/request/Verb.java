package org.academiadecodigo.bootcamp.webserver.request;

public enum Verb {
    GET,
    UNSUPPORTED;

    public static Verb getFromString(String string) {
        for (Verb verb : values()) {
            if (string.equals(verb.toString())) {
                return verb;
            }
        }

        return UNSUPPORTED;
    }
}
