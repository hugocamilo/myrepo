package org.academiadecodigo.workspace.mirc;

import java.io.IOException;
import java.net.ServerSocket;
import java.util.LinkedList;
import java.util.concurrent.ExecutorService;

public class Server {

    private LinkedList<ClientConnection> clientConnectionList;
    private ExecutorService clientPool;
    private ServerSocket socket;

    public static void main(String[] args) {
        try {

            Server server = new Server(8080);
            server.start();
            Client pessoa = new Client();
            Tiago tiago = new Tiago();

        } catch (IOException ex) {

            System.out.println("IOException caught " + ex.getMessage());
        }

    }



    public Server(int port) throws IOException {

        socket = new ServerSocket(port);
        clientConnectionList = new LinkedList<>();
    }

    public void start() {

        while (!socket.isClosed()) {

            try {
                ClientConnection clientConnection = new ClientConnection(socket.accept(),this);
                clientPool.submit(clientConnection);
                clientConnectionList.add(clientConnection);

            } catch (IOException e) {

                System.out.println("Error handling client request: " + e.getMessage());
            }
        }

    }
}
