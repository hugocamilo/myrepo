package org.academiadecodigo.stringteasers.concurrencyhandson.explicitcreation;

public class ThreadLauncher {


    public static void main(String[] args) {

        MyThread myThread = new MyThread();

        myThread.run();
//
//        for (int i = 0; i < 10; i++) {
//            Thread thread = new Thread(new MyThread());
////            thread.setName("T" + i);
//            thread.start();
//        }

        Thread thread = new Thread(myThread);
        thread.start();

    }
}
