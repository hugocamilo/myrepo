package org.academiadecodigo.stringteasers.concurrencyhandson.explicitcreation;

public class MyThread implements Runnable {
    @Override
    public void run() {

        System.out.println(Thread.currentThread().getName());
    }
}
