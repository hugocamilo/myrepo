package org.academiadecodigo.stringteasers.concurrencyhandson.threadsync;

public class FabricaDasCaldas {


    public static void main(String[] args) {

        System.out.println("Gerente: Estou aqui à espera que estes colaboradores acabem.");

        Thread trabalhador = new Thread(new TrabalhadorDasCaldas());
        trabalhador.start();

        try {
            trabalhador.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        System.out.println("GERENTE: Acabou o trabalho siga beber uma...");

    }
}
