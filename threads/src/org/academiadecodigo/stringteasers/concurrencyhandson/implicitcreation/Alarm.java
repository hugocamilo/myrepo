package org.academiadecodigo.stringteasers.concurrencyhandson.implicitcreation;

import java.util.Scanner;
import java.util.Timer;
import java.util.TimerTask;

public class Alarm {

    private Timer timer;

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        System.out.println("Number of rings?");
        int numRings = Integer.parseInt(scanner.next());

        System.out.println("Ring interval in seconds?");
        int ringInterval = Integer.parseInt(scanner.next());

        System.out.println(Thread.currentThread().getName());

        Alarm alarm = new Alarm();
        alarm.start(ringInterval, numRings);

        Alarm secondAlarm = new Alarm();
        secondAlarm.start(ringInterval, numRings);

    }

    private void start(int ringInterval, int numRings) {

        timer = new Timer();
        timer.scheduleAtFixedRate(new RingRing(numRings), ringInterval * 1000, ringInterval * 1000);
    }

    private class RingRing extends TimerTask {

        private int numRings;

        public RingRing(int numRings) {
            this.numRings = numRings;
        }

        @Override
        public void run() {

            System.out.println(Thread.currentThread().getName());
            System.out.println("RING! RING! RING! gollum");

            numRings--;

            if (numRings == 0) {
                System.out.println("org.academiadecodigo.stringteasers.concurrencyhandson.implicitcreation.Alarm Cancelled");
                stop();
            }

        }

        private void stop() {
            timer.cancel();
        }
    }
}
