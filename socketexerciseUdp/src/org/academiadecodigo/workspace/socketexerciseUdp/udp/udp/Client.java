package org.academiadecodigo.workspace.socketexerciseUdp.udp.udp;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.UnknownHostException;

public class Client {
    public static void main(String[] args) {
        Client client = new Client();

        String destinationHost = "127.0.0.1";
        int destinationPort = 8080;
        int sourcePort = 8081;
        byte[] sendBuffer;
        byte[] recvBuffer = new byte[1024];
        String message = "teste";
        sendBuffer = message.getBytes();
        client.start(sendBuffer,destinationHost,destinationPort, sourcePort, recvBuffer);


    }
    public void start(byte[] sendBuffer,String destinationHost, int destinationPort, int sourcePort, byte[] recvBuffer){
        try{
            DatagramSocket socket = new DatagramSocket(sourcePort);
            DatagramPacket sendPacket = new DatagramPacket(sendBuffer,
                    sendBuffer.length, InetAddress.getByName(destinationHost), destinationPort);
            socket.send(sendPacket);


            DatagramPacket receivePacket = new DatagramPacket(recvBuffer, recvBuffer.length, sendPacket.getAddress(), sendPacket.getPort());
            socket.receive(receivePacket);
            System.out.println("here");
            String message = new String(recvBuffer, 0,receivePacket.getLength());
            System.out.println(message);
            socket.close();




        }catch (UnknownHostException e){
            System.out.println("unknown host exception "+e);
        }catch (IOException e){
            System.out.println("IO exception "+e);
        }

    }
}
