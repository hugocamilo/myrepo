package org.academiadecodigo.workspace.socketexerciseUdp.udp.udp;


import java.io.IOException;
import java.net.*;

public class Server {
    public static void main(String[] args) {

        String hostName = "127.0.0.1";
        int portNumber = 8080;
        byte[] sendBuffer = new byte[1024];
        byte[] recvBuffer = new byte[1024];

        Server server = new Server();
        server.start(portNumber,sendBuffer,recvBuffer, hostName);

    }

    private void start(int portNumber, byte[] sendBuffer, byte[] recvBuffer, String hostName){

        DatagramSocket socket = null;
        String message = "";
        //RECIEVE//

        try{
            socket = new DatagramSocket(portNumber);
            DatagramPacket receivePacket = new DatagramPacket(recvBuffer, recvBuffer.length);
            socket.receive(receivePacket);
            message = new String(recvBuffer,0,receivePacket.getLength()).toUpperCase();
            sendBuffer = message.getBytes();


            DatagramPacket sendPacket = new DatagramPacket(sendBuffer,sendBuffer.length, receivePacket.getAddress(), receivePacket.getPort());
            socket.send(sendPacket);
            socket.close();

        } catch (SocketException e) {
            System.out.println("socket exception " + e);
        } catch (IOException e) {
            System.out.println("IO exception " + e);
        }

    }
     /*
            DatagramPacket receivePacket = new DatagramPacket(recvBuffer, recvBuffer.length);
            socket = new DatagramSocket(portNumber);
            socket.receive(receivePacket);

            socket = new DatagramSocket(portNumber);
            DatagramPacket sendPacket = new DatagramPacket(sendBuffer,
                    sendBuffer.length, InetAddress.getByName(hostName), portNumber); socket.send(sendPacket);*/

}
