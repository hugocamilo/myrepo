package org.academiadecodigo.workspace.socketexerciseUdp.udp.tcp;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;

public class Client {
    //system.in
    //reader
    //writer
    //print to terminal
    /*
    source ip
    destination ip
    source port
    destination port
    */
    public static void main(String[] args) {

        Client client = new Client();
        InetAddress sourceIp;

        try {
            sourceIp = InetAddress.getByName(client.getClass().getName());
            int sourcePort = 8081;
            String destinationIp = "127.0.0.1";
            int destinationPort = 8080;
            client.init(destinationIp, destinationPort);
        } catch (IOException e) {
            System.out.println("IOException " + e);
        }

    }
        public void init (String destinationIp,int destinationPort)throws IOException{

            Socket clientSocket = new Socket(destinationIp, destinationPort);

            PrintWriter out = new PrintWriter(clientSocket.getOutputStream(), true);
            BufferedReader in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
        }

    }
