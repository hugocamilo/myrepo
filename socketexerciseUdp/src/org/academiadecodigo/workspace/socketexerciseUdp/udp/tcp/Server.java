package org.academiadecodigo.workspace.socketexerciseUdp.udp.tcp;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;

public class Server {
    public static void main(String[] args) {
        //reader
        //writer
        //print to terminal

        /*
        source ip
        destination ip
        source port
        destination port
        */

        String destinationIp = "127.0.0.1";
        int portNumber = 8080;
        Server server = new Server();
        try {
            server.init(portNumber);
        } catch (IOException e) {
            System.out.println("error IOException " + e);
        }

    }

    public void init(int portNumber) throws IOException {

        ServerSocket serverSocket = new ServerSocket(portNumber);
        Socket clientSocket = serverSocket.accept();
        InetAddress sourceIp = clientSocket.getInetAddress();
        int sourcePort = clientSocket.getPort();
        PrintWriter out = new PrintWriter(clientSocket.getOutputStream(), true);
        BufferedReader in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
    }


}
