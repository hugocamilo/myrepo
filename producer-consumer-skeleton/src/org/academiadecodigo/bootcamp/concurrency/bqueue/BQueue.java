package org.academiadecodigo.bootcamp.concurrency.bqueue;

import java.util.LinkedList;

/**
 * Blocking Queue
 *
 * @param <T> the type of elements stored by this queue
 */
public class BQueue<T> {

    private LinkedList<T> list;
    private int limit;

    /**
     * Constructs a new queue with a maximum size
     *
     * @param limit the queue size
     */
    public BQueue(int limit) {

        list = new LinkedList<>();
        this.limit = limit;
    }

    /**
     * Inserts the specified element into the queue
     * Blocking operation if the queue is full
     *
     * @param data the data to add to the queue
     */
    public synchronized void offer(T data) {
        while (list.size() == getLimit())
            try {
                System.out.println("NAO HA ESPAÇO!!");
                wait();
            } catch (InterruptedException ex) {
                System.out.println(ex.getMessage());
            }
        notifyAll();
        list.addLast(data);
        System.out.println("toma la disto");

    }

    /**
     * Retrieves and removes data from the head of the queue
     * Blocking operation if the queue is empty
     *
     * @return the data from the head of the queue
     */
    public synchronized T poll() {

        while (list.isEmpty()) {
            try {
                System.out.println("ENTAO MAS NAO HA PITEU NESTA MERDA?!?!");
                wait();

            } catch (InterruptedException ex) {
                System.out.println(ex.getMessage());
            }
        }
        notifyAll();
        System.out.println("da ca disto!!");
        return list.removeFirst();

    }

    /**
     * Gets the number of elements in the queue
     *
     * @return the number of elements
     */
    public int getSize() {

        return list.size();
        //throw new UnsupportedOperationException();

    }

    /**
     * Gets the maximum number of elements that can be present in the queue
     *
     * @return the maximum number of elements
     */

    public int getLimit() {

        return limit;
        //throw new UnsupportedOperationException();

    }

}
