package org.academiadecodigo.bootcamp.concurrency;

import org.academiadecodigo.bootcamp.concurrency.bqueue.BQueue;

import java.util.Random;

/**
 * Consumer of integers from a blocking queue
 */
public class Consumer implements Runnable {

    private final BQueue<Integer> queue;
    private volatile int elementNum;
    Thread.State state;

    /**
     * @param queue      the blocking queue to consume elements from
     * @param elementNum the number of elements to consume
     */
    public Consumer(BQueue queue, int elementNum) {
        this.queue = queue;
        this.elementNum = elementNum;

    }

    @Override
    public void run() {
        /*try {
            for (int i = 0; i < elementNum; i++) {
                Thread.sleep(100, (int) Math.random());
                queue.poll();
            }
        }catch (InterruptedException ex){
            System.out.println(ex.getMessage());
        }*/
        while(elementNum > 0){
            try{
                Thread.sleep(100,(int)Math.random());
                queue.poll();
                elementNum--;
            }catch (InterruptedException ex){
                System.out.println(ex.getMessage());
            }
        }

    }



}
