package org.academiadecodigo.bootcamp.concurrency;

import org.academiadecodigo.bootcamp.concurrency.bqueue.BQueue;

import java.util.Random;

/**
 * Produces and stores integers into a blocking queue
 */
public class Producer implements Runnable {

    private final BQueue<Integer> queue;
    private volatile int elementNum;

    /**
     * @param queue      the blocking queue to add elements to
     * @param elementNum the number of elements to produce
     */
    public Producer(BQueue queue, int elementNum) {
        this.queue = queue;
        this.elementNum = elementNum;
    }

    @Override
    public void run() {
        while(elementNum > 0){
            try{
                Thread.sleep(100, (int)Math.random());
            }catch (InterruptedException ex){
                System.out.println(ex.getMessage());
            }
            queue.offer(elementNum);
            elementNum--;
        }


    }


}
