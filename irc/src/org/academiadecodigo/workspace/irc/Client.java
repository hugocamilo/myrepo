package org.academiadecodigo.workspace.irc;

import java.io.*;
import java.net.Socket;

public class Client {

    private DataInputStream inputStream;
    private DataOutputStream outputStream;
    private Socket serverSocket;

    public static void main(String[] args) {
        Client client = new Client();
        client.start();
    }

    public Client() {
        start();

    }

    private void setupComs() {
        System.out.println("Trying to set up coms...");
        try {
            serverSocket = new Socket("127.0.0.1", 8080);
            System.out.println("connected to host: " + serverSocket.getInetAddress() + " on port: " + serverSocket.getPort());

            System.out.println("setting up buffered writer...");
            //writer = new BufferedWriter(new OutputStreamWriter(serverSocket.getOutputStream()));

            inputStream = new DataInputStream(System.in);

            // sends output to the socket
            outputStream = new DataOutputStream(serverSocket.getOutputStream());
            System.out.println("buffered writer set.");

            System.out.println("setting up buffered reader...");
            //reader = new BufferedReader(new InputStreamReader(System.in));
            System.out.println("reader set.");

        } catch (IOException ex) {
            System.out.println(ex.getStackTrace().toString());
        }
    }

    private void start() {
        setupComs();
        while (serverSocket.isBound()) {
            run();
            System.out.println("running...");
        }

    }

    private void run() {
        sendMessage();
        System.out.println("listening to send");
        receiveMessage();
        System.out.println("listening to receive");

    }

    private void receiveMessage() {
        String line = "";
        try {
            DataInputStream serverInput = new DataInputStream(serverSocket.getInputStream());
            while (!line.equals(null)) {
                line = serverInput.readLine();
                System.out.println(line);
            }
        } catch (IOException ex) {
            System.out.println(ex.getStackTrace().toString());
        }
    }

    private void sendMessage() {
        String line = "";
        while (!line.equals(null)) {
            try {
                line = inputStream.readLine();
                outputStream.writeUTF(line);
            } catch (IOException ex) {
                System.out.println(ex.getStackTrace().toString());
            }
        }
    }
    public void receiveBroadCast(String message){
        System.out.println(message);
    }
}
