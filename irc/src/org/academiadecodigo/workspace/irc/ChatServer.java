package org.academiadecodigo.workspace.irc;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ChatServer {

    private DataInputStream inputStream;
    private DataOutputStream outputStream;
    private ServerSocket serverSocket;
    private Socket clientSocket;
    private ExecutorService executorThread;
    private CopyOnWriteArrayList<ClientConnection> clientList = new CopyOnWriteArrayList<>();

    public static void main(String[] args) {
        ChatServer chatServer = new ChatServer();
    }

    public ChatServer() {

        start();
        run();
    }

    public void start() {
        try {
            System.out.println("Setting up server on port: 8080");
            serverSocket = new ServerSocket(8080);
            System.out.println("Coms are set.");

        } catch (IOException ex) {
            System.out.println(ex.getStackTrace().toString());
        }

    }

    private void setupComs() throws IOException {
        inputStream = new DataInputStream(new BufferedInputStream(clientSocket.getInputStream()));
        outputStream = new DataOutputStream(clientSocket.getOutputStream());
    }

    public void run() {
        while (true) {
            try {
                System.out.println("waiting for client to connect...");
                clientSocket = serverSocket.accept();
                if (serverSocket.isBound()) {
                    System.out.println("Client is connected");
                    System.out.println("server socket status : " + serverSocket.isBound());
                    setupComs();
                    ClientConnection clientConnection = new ClientConnection(this, clientSocket, inputStream, outputStream);
                    clientList.add(clientConnection);
                    System.out.println("client connection created.");
                    executorThread = Executors.newCachedThreadPool();
                    executorThread.submit(clientConnection);
                    System.out.println("thread created.");
                    System.out.println("coms are set.");
                    readFromClient();
                    System.out.println("done reading");
                }
            } catch (IOException ex) {
                System.out.println(ex.getStackTrace().toString());
            }
        }
    }

    private void readFromClient() throws IOException {
        String line = "";
        while (!line.equals("Over")) {
            line = inputStream.readUTF();
            for (ClientConnection clientConnection : clientList) {
                clientConnection.broadCast(line);
            }
        }
    }
}
