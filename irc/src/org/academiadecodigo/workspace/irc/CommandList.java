package org.academiadecodigo.workspace.irc;

public enum  CommandList {
    WHISPER,
    SET_USER,
    REGISTER_USER,
}
