package org.academiadecodigo.workspace.irc;

import java.io.*;
import java.net.Socket;

public class ClientConnection implements Runnable {

    private ChatServer server;
    private Socket client;
    private DataInputStream inputStream;
    private DataOutputStream outputStream;
    private String text;

    public ClientConnection(ChatServer server, Socket client, DataInputStream inputStream, DataOutputStream outputStream){
        this.server = server;
        this.client = client;
        this.inputStream = inputStream;
        this.outputStream = outputStream;
    }
    public void broadCast(String message)throws IOException {

        System.out.println(message);

    }
    private void displayMessage(){
        System.out.println();
    }
    @Override
    public void run() {

    }
}
