package org.academiadecodigo.workspace.mapeditor;
import org.academiadecodigo.simplegraphics.graphics.Color;
import java.io.*;
import java.util.LinkedList;

public class MapEditor {
    //PROPERTIES
    private int row;
    private int col;
    public static int MAX_COLS;
    public static int MAX_ROWS;
    private boolean load;
    private Cell[][] cells;
    private LinkedList<Cell> cellList;
    private Pointer pointer;

    //CONSTRUCTOR//
    public MapEditor(int row, int col) {

        this.row = row;
        this.col = col;
        cells = new Cell[row][col];
        MAX_COLS = col;
        MAX_ROWS = row;
        createCell();
        pointer = new Pointer(0, 0, cells, this);
        cellList = new LinkedList<>();
        load = false;
    }

    //LOAD METHOD
    //PRESS T KEY TO LOAD FILE
    public void loadFile() {

        try {
            if (!load) {
                String file = "C:\\Users\\Cat Motel\\Desktop\\mapEditor\\src\\org\\academiadecodigo\\workspace\\mapeditor\\fileToLoad";
                FileReader reader = new FileReader(file);
                BufferedReader bReader = new BufferedReader(reader);
                String line = bReader.readLine();
                String[] split;
                if (line == null) {
                    return;
                }
                split = line.split(" ");
                this.row = Integer.parseInt(split[0]);
                this.col = Integer.parseInt(split[1]);
                MAX_COLS = col;
                MAX_ROWS = row;
                while ((line = bReader.readLine()) != null) {
                    split = line.split(" ");
                    cells[Integer.parseInt(split[0])][Integer.parseInt(split[1])].setSelected(true);
                    cellList.add(cells[Integer.parseInt(split[0])][Integer.parseInt(split[1])]);
                    cells[Integer.parseInt(split[0])][Integer.parseInt(split[1])].getRectangle().setColor(Color.MAGENTA);
                    cells[Integer.parseInt(split[0])][Integer.parseInt(split[1])].getRectangle().fill();
                }
                load = true;
                bReader.close();
            }
        } catch (Exception e) {
        }
        load = false;
    }

    //SAVE METHOD
    //PRESS R KEY TO SAVE FILE
    public void saveFile() throws IOException {

        load = false;
        String file = "C:\\Users\\Cat Motel\\Desktop\\mapEditor\\src\\org\\academiadecodigo\\workspace\\mapeditor\\fileToLoad";
        PrintWriter writer = new PrintWriter(file);
        int i = 0;
        writer.println(MAX_ROWS + " " + MAX_COLS);
        while (i < cellList.size()) {
            writer.println(cellList.get(i).getRow() + " " + cellList.get(i).getCol());
            i++;
        }
        writer.close();
    }

    //RESET METHOD
    //PRESS Q KEY TO RESET CANVAS
    public void clear() {

        for (int x = 0; x < row; x++) {
            for (int y = 0; y < col; y++) {
                cells[x][y].setSelected(false);
                cells[x][y].getRectangle().setColor(Color.BLACK);
                cells[x][y].getRectangle().draw();
            }
        }
        cellList = new LinkedList<>();
    }

    //CHECK STATE FUNCTION
    //function that checks the current cell for its state and adds/removes it from the linked list
    //that we use to store the differences between states
    public void checkState(Cell cell) {

        if (cell.isSelected() && !cellList.contains(cell)) {
            cellList.add(cell);
        } else if (cellList.contains(cell) && !pointer.isSelection()) {
            cellList.remove(cell);
        }
    }

    //INIT METHOD
    //Initializes the Pointer class
    public void init() {
        pointer.init();
    }

    //PROGRAM LOOP
    //Checks the cell "underneath" our pointer, on its current position, for its current state and paints it accordingly
    //MAGENTA fill => Selected
    //BLACK draw => !Selected
    public void start(){

        while (true) {
            if (pointer.isSelection()) {
                cells[pointer.getRow()][pointer.getCol()].setSelected(true);
                cells[pointer.getRow()][pointer.getCol()].getRectangle().setColor(Color.MAGENTA);
                cells[pointer.getRow()][pointer.getCol()].getRectangle().fill();
            } else {
                cells[pointer.getRow()][pointer.getCol()].setSelected(false);
                cells[pointer.getRow()][pointer.getCol()].getRectangle().setColor(Color.BLACK);
                cells[pointer.getRow()][pointer.getCol()].getRectangle().draw();

            }
            checkState(cells[pointer.getRow()][pointer.getCol()]);
        }
    }

    //CREATE CELL
    //Nested for loop that iterates our grid and adds a new cell per coordinate
    public void createCell() {

        for (int x = 0; x < row; x++) {
            for (int y = 0; y < col; y++) {
                cells[x][y] = new Cell(x, y);
            }
        }
    }
}
