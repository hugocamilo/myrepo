package org.academiadecodigo.workspace.mapeditor;

import org.academiadecodigo.simplegraphics.graphics.Color;
import org.academiadecodigo.simplegraphics.graphics.Rectangle;

public class Cell {
    //PROPERTIES
    private int row;
    private int col;
    public static final int PADDING = 10;
    public static final int CELL_SIZE = 30;
    private Color color;
    private boolean selected;
    private Rectangle rectangle;

    //CONSTRUCTOR
    public Cell(int x, int y) {

        this.row = x;
        this.col = y;
        selected = false;
        rectangle = new Rectangle(x*CELL_SIZE+PADDING,y*CELL_SIZE+PADDING,CELL_SIZE,CELL_SIZE);
        rectangle.draw();
    }

    //GETTERS && SETTERS
    public int getRow() {
        return row;
    }
    public void setRow(int row) {

        this.row = row;
        System.out.println("x :" +getRow() + " y: " +getCol());
    }
    public int getCol() {
        return col;
    }
    public void setCol(int col) {

        this.col = col;

        System.out.println("x :" +getRow() + " y: " +getCol());
    }
    public Rectangle getRectangle() {
        return rectangle;
    }
    public boolean isSelected() {
        return selected;
    }
    public void setSelected(boolean selected) {
        this.selected = selected;
    }
}
