package org.academiadecodigo.workspace.mapeditor;
import org.academiadecodigo.simplegraphics.graphics.Color;
import org.academiadecodigo.simplegraphics.graphics.Rectangle;
import org.academiadecodigo.simplegraphics.keyboard.Keyboard;
import org.academiadecodigo.simplegraphics.keyboard.KeyboardEvent;
import org.academiadecodigo.simplegraphics.keyboard.KeyboardEventType;
import org.academiadecodigo.simplegraphics.keyboard.KeyboardHandler;

import java.io.FileNotFoundException;
import java.io.IOException;

public class Pointer extends Cell implements KeyboardHandler {
    private Cell[][] cells;
    private Cell cell;
    private boolean selection;
    private Rectangle rectangle;
    private MapEditor mp;
    private Keyboard keyboard = new Keyboard(this);

    //CONSTRUCTOR//
    public Pointer(int x, int y, Cell[][] cells, MapEditor mp) {

        super(x, y);
        this.mp = mp;
        selection = false;
        this.cells = cells;
        this.cell = cells[this.getCol()][this.getRow()];
        rectangle = new Rectangle(x * CELL_SIZE + PADDING, y * CELL_SIZE + PADDING, CELL_SIZE, CELL_SIZE);
        rectangle.setColor(Color.GREEN);
        rectangle.fill();
    }
    @Override
    public void keyReleased(KeyboardEvent keyboardEvent) {
    }

    @Override
    public void keyPressed(KeyboardEvent keyboardEvent) {

        switch (keyboardEvent.getKey()) {

            case KeyboardEvent.KEY_W:
                moveUp();
                break;

            case KeyboardEvent.KEY_S:
                moveDown();
                break;
            case KeyboardEvent.KEY_A:
                moveLeft();
                break;
            case KeyboardEvent.KEY_D:
                moveRight();
                break;
            case KeyboardEvent.KEY_F:
                toggleSelection();
                break;
            case KeyboardEvent.KEY_R:
                saveFile();
                break;
            case KeyboardEvent.KEY_T:
                    readFile();
                break;
            case KeyboardEvent.KEY_Q:
                clear();
                break;
        }
    }

    //POINTER FUNCTIONALITY METHODS
    public void clear(){

        mp.clear();
    }
    public void readFile(){

        mp.loadFile();
    }
    public void saveFile() {

        try {
            System.out.println("save call");
            mp.saveFile();
        } catch (IOException e) {
            System.out.println("save");
            System.out.println("error " + e);
        }

    }
    public boolean isSelection() {

        return selection;
    }

    //POINTER MOVEMENT METHODS
    public void moveUp() {

        if (this.getCol() - 1 >= 0) {
            this.setCol(getCol() - 1);
            rectangle.translate(0, -1 * getRectangle().getWidth());
        }
    }
    public void moveDown() {

        if (this.getCol() + 1 < MapEditor.MAX_COLS) {
            this.setCol(getCol() + 1);
            rectangle.translate(0, +1 * getRectangle().getWidth());
        }
    }
    public void moveLeft() {

        if (this.getRow() - 1 >= 0) {
            this.setRow(getRow() - 1);
            rectangle.translate(-1 * getRectangle().getWidth(), 0);
        }
    }
    public void moveRight() {

        if (this.getRow() + 1 < MapEditor.MAX_ROWS) {
            this.setRow(getRow() + 1);
            rectangle.translate(+1 * getRectangle().getWidth(), 0);
        }
    }
    public void toggleSelection() {

        selection = !selection;
        mp.checkState(cells[this.getRow()][this.getCol()]);
    }


    //INITIALIZE METHOD
    public void init() {
        KeyboardEvent select = new KeyboardEvent();
        select.setKey(KeyboardEvent.KEY_F);
        select.setKeyboardEventType(KeyboardEventType.KEY_PRESSED);

        KeyboardEvent left = new KeyboardEvent();
        left.setKey(KeyboardEvent.KEY_A);
        left.setKeyboardEventType(KeyboardEventType.KEY_PRESSED);

        KeyboardEvent up = new KeyboardEvent();
        up.setKey(KeyboardEvent.KEY_W);
        up.setKeyboardEventType(KeyboardEventType.KEY_PRESSED);

        KeyboardEvent down = new KeyboardEvent();
        down.setKey(KeyboardEvent.KEY_S);
        down.setKeyboardEventType(KeyboardEventType.KEY_PRESSED);

        KeyboardEvent right = new KeyboardEvent();
        right.setKey(KeyboardEvent.KEY_D);
        right.setKeyboardEventType(KeyboardEventType.KEY_PRESSED);

        KeyboardEvent save = new KeyboardEvent();
        save.setKey(KeyboardEvent.KEY_R);
        save.setKeyboardEventType(KeyboardEventType.KEY_PRESSED);

        KeyboardEvent load = new KeyboardEvent();
        load.setKey(KeyboardEvent.KEY_T);
        load.setKeyboardEventType(KeyboardEventType.KEY_PRESSED);

        KeyboardEvent clear = new KeyboardEvent();
        clear.setKey(KeyboardEvent.KEY_Q);
        clear.setKeyboardEventType(KeyboardEventType.KEY_PRESSED);

        keyboard.addEventListener(left);
        keyboard.addEventListener(right);
        keyboard.addEventListener(up);
        keyboard.addEventListener(down);
        keyboard.addEventListener(select);
        keyboard.addEventListener(save);
        keyboard.addEventListener(load);
        keyboard.addEventListener(clear);
    }
}
