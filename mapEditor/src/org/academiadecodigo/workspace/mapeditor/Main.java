package org.academiadecodigo.workspace.mapeditor;

public class Main {
    public static void main(String[] args) throws InterruptedException {
        MapEditor mp = new MapEditor(20,20);
        mp.init();
        mp.start();
    }
}
