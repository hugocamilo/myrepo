package org.academiadecodigo.workspace.curl;
import java.io.*;
import java.net.MalformedURLException;
import java.net.URL;

public class Main {
    public static void main(String[] args) throws IOException {
        URL url;
        String line = "";
        String input ="";
        BufferedReader bufferedReader;
        BufferedReader inputReader = new BufferedReader(new InputStreamReader(System.in));
        try{
            System.out.println("Type the URL");
            input = inputReader.readLine();
            //url = new URL(input);
            url = new URL("HTTP",input,80,"");
            bufferedReader = new BufferedReader(new InputStreamReader(url.openStream()));
            while ((line = bufferedReader.readLine()) != null){
                //result += line;
                System.out.println(line);
            }
            bufferedReader.close();
        }catch (MalformedURLException ex){
            System.out.println("Malformed exception " +ex.getMessage());
        }catch (IOException ex){
            System.out.println("IOException found "+ex.getMessage());
        }
        finally {
            inputReader.close();

        }
    }
}
