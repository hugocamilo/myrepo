package org.academiadecodigo.workspace.webserver;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.logging.Logger;

public class WebServer {

    ServerSocket serverSocket;
    Socket clientSocket;
    Logger logger;

    public static void main(String[] args) {
        //setup(); setup sockets
        //listen();
        //readRequest();
        //getResponse();
        //serveResponse();
        WebServer webServer = new WebServer();
        webServer.start();
    }


    private void start() {
        System.out.println("Web Server starting");
        System.out.println("Waiting connection...");
        while(true){
            setup();
            listen();
        }


    }

    private void setup() {
        try {
            serverSocket = new ServerSocket(8080);
            System.out.println("now listening on port " + serverSocket.getLocalPort());

        } catch (IOException ex) {

            System.out.println("IOException caught " + ex.getMessage());
        }

    }

    private void listen() {
        try {

            System.out.println("Listening...");
            clientSocket = serverSocket.accept();
            System.out.println("Connection accepted");
            if (clientSocket.isBound()) {
                readRequest();
            }

        } catch (IOException ex) {

            System.out.println("IOException caught " + ex.getMessage());
        }
    }

    private void readRequest() {
        String line = "";
        String method = "";
        String index = "";
        BufferedReader bufferedReader;
        try {
            bufferedReader = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
            System.out.println(bufferedReader);
            line = bufferedReader.readLine();
            System.out.println(line);
            if(line.contains("POST")){
                validadeMethod();
            }
            if (line.contains("GET")) {

                if (line.contains("index")) {
                    validateIndex("GET");
                } else {
                    validadeResource("not found");
                }
            }
        } catch (IOException ex) {
            System.out.println("IOException caught " + ex.getMessage());
        }
    }

    private void validateIndex(String method) {
        if (method.equals("GET")) {
            try {
                File file = new File("www/index.html");
                file.length();
                String readFile;
                BufferedReader bufferedReader = new BufferedReader(new FileReader(file));

                DataOutputStream out = new DataOutputStream(clientSocket.getOutputStream());
                out.writeBytes("HTTP/1.1 200 OK\r\n");
                out.writeBytes("Content-Type: text/html\r\n");
                out.writeBytes("Content-Length: " + file.length() + "\r\n\r\n");
                while ((readFile = bufferedReader.readLine()) != null) {
                    out.writeBytes(readFile);
                }
            } catch (IOException ex) {
                System.out.println("IOException caught " + ex.getMessage());
            }
        }

    }

    private void validadeResource(String resource) {
        if (resource.equals("not found")) {
            try {
                File file = new File("www/404.html");
                file.length();
                String readFile;
                BufferedReader bufferedReader = new BufferedReader(new FileReader(file));

                DataOutputStream out = new DataOutputStream(clientSocket.getOutputStream());
                out.writeBytes("HTTP/1.1 404 Not Found\r\n");
                out.writeBytes("Content-Type: text/html\r\n");
                out.writeBytes("Content-Length: " + file.length() + "\r\n\r\n");
                while ((readFile = bufferedReader.readLine()) != null) {
                    out.writeBytes(readFile);
                }
            } catch (IOException ex) {
                System.out.println("IOException caught " + ex.getMessage());
            }

        }
    }

    private void validadeMethod(){
        try {
            File file = new File("www/405.html");
            file.length();
            String readFile;
            BufferedReader bufferedReader = new BufferedReader(new FileReader(file));

            DataOutputStream out = new DataOutputStream(clientSocket.getOutputStream());
            out.writeBytes("HTTP/1.1 405 Not Allowed\r\n");
            out.writeBytes("Content-Type: text/html\r\n");
            out.writeBytes("Content-Length: " + file.length() + "\r\n\r\n");
            while ((readFile = bufferedReader.readLine()) != null) {
                out.writeBytes(readFile);
            }
        } catch (IOException ex) {
            System.out.println("IOException caught " + ex.getMessage());
        }
    }

    private void closeConnections() {
        try {
            clientSocket.close();
        } catch (IOException ex) {
            System.out.println("IOException caught " + ex.getMessage());
        }

    }
}
